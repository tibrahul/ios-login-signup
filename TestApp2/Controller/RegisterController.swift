//
//  RegisterController.swift
//  TestApp2
//
//  Created by 10decoders on 10/03/18.
//  Copyright © 2018 10Decoders. All rights reserved.
//

import UIKit
import RealmSwift

class RegisterController: UIViewController {
    
    let relam = try! Realm()
    let user = User()

    @IBOutlet weak var userIdText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var rePasswordText: UITextField!
    @IBOutlet weak var fNameText: UITextField!
    @IBOutlet weak var lNameText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        
        if(passwordText.text == rePasswordText.text) {
            user.userid = userIdText.text!
            user.email = emailText.text!
            user.password = passwordText.text!
            user.fname = fNameText.text!
            user.lname = lNameText.text!
            user.age = Int(ageText.text!)!
            user.phone = Int(phoneText.text!)!
          
            print(userIdText.text!)
            
            let userObject = User(value: ["userid" : user.userid, "email" : user.email, "password" : user.password, "fname" : user.fname, "lname" : user.lname, "age" : user.age, "phone" : user.phone])
            
            try! relam.write {
                print("success")
                relam.add(userObject)
                
            }
           
        } else {
            print("unsuccessfull")
        }
    }
    
}
