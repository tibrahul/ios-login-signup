//
//  LoginController.swift
//  TestApp2
//
//  Created by 10decoders on 10/03/18.
//  Copyright © 2018 10Decoders. All rights reserved.
//

import UIKit
import RealmSwift

class LoginController: UIViewController {
    let relam = try! Realm()
    
    @IBOutlet weak var userIDText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    print(Realm.Configuration.defaultConfiguration.fileURL!)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        let result  = relam.objects(User.self).filter("userid == %@ AND password== %@",userIDText.text!, passwordText.text!)
        if(result.count > 0) {
            performSegue(withIdentifier: "dashboardView", sender: self)
        }
        
    }
    
}
