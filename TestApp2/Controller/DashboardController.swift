//
//  DashboardController.swift
//  TestApp2
//
//  Created by 10decoders on 11/03/18.
//  Copyright © 2018 10Decoders. All rights reserved.
//

import UIKit
import Alamofire

class DashboardController: UIViewController {

    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveInfoAction(_ sender: UIButton) {
        print("nameText", nameText.text!)
        print("ageText", ageText.text!)
        
        let parameters = [
            "name" : nameText.text!,
            "age" : ageText.text!
        ]
        Alamofire.request("http://localhost:3000/api/Student_Default_Activity/Student", method: .post, parameters: parameters)
//        Alamofire.request("http://localhost:3000/api/Student_Default_Activity/Student", parameters: parameters).responseJSON { request, response, JSON, error in
//            print(response)
//            print(JSON)
//            print(error)
//        }


    }

}
