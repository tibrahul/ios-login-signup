//
//  User.swift
//  TestApp2
//
//  Created by 10decoders on 10/03/18.
//  Copyright © 2018 10Decoders. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    @objc dynamic var userid: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var fname: String = ""
    @objc dynamic var lname: String = ""
    @objc dynamic var age: Int = 0
    @objc dynamic var phone: Int  = 0
    
}
